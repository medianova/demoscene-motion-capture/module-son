$(document).ready(function() {
    musicList = ["/sons/1.mp3", "/sons/2.mp3", "/sons/3.mp3"];

    function start(){
        audio = $("audio");
        audio.on("ended", randomSong());
        audio.trigger("play");
        // $( "#start" ).toggle( "highlight" );
    }

    function randomSong(){
        audio.attr("src", musicList[Math.floor(Math.random() * (musicList.length - 0 + 1) + 0)]);
    }

    $("#start").on("click", function(){
        if(typeof audio != 'undefined'){
           fadeOutEffect();
        }
        fadeOutEffect();
        start();
        audio.prop("volume", 1);
    });

    function fadeOutEffect() {
        var fadeTarget = $("video");
        var fadeEffect = setInterval(function () {

            if (audio.prop("volume") >= 0 && audio.prop("volume") < 1)
            {
                audio.prop("volume") -= 0.1;
                console.log(audio.prop("volume"), " coucou")
            }

        }, 200);
    }
});